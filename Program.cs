﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace BasicBlackJack
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }

    public enum EnumValue
    {
        Null = 0,
        A = 1,
        Two = 2,
        Three = 3,
        Four = 4,
        Five = 5,
        Six = 6,
        Seven = 7,
        Eight = 8,
        Nine = 9,
        Ten = 10,
        J = 11,
        Q = 12,
        K = 13
    }

    public enum EnumSuit
    {
        Null = 0,
        Spades = 1,
        Diamonds = 2,
        Clubs = 3,
        Hearts = 4
    }

    public class Card : IEquatable<Card>, IComparable<Card>
    {
        public EnumValue Value { get; set; }
        public EnumSuit Suit { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }

        public override string ToString()
        {
            return Name;
        }

        #region source: https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.list-1.sort?view=net-5.0
        public override bool Equals(object _obj)
        {
            if (_obj == null) 
                return false;

            Card objAsCard = _obj as Card;

            if (objAsCard == null) 
                return false;

            else 
                return Equals(objAsCard);
        }

        public bool Equals(Card _other)
        {
            if (_other == null) 
                return false;
            return (Value.Equals(_other.Value));
        }

        public int CompareTo(Card _compareCard)
        {
            if (_compareCard == null)
            {
                return 1;
            }
            else
            {
                return Value.CompareTo(_compareCard.Value);
            }
        }

        public override int GetHashCode()
        {
            return (int)Value * 10 + (int)Suit;
        }
        #endregion

        public Card()
        {
            Value = EnumValue.Null;
            Suit = EnumSuit.Null;
            Name = string.Empty;
            Image = string.Empty;
        }

        public Card(EnumValue _value, EnumSuit _suit)
        {
            string ShortSuit = "";
            string ImageValue = "";

            if (_suit == EnumSuit.Null)
            {
                throw new ArgumentNullException("Suit");
            }

            if (_value == EnumValue.Null)
            {
                throw new ArgumentNullException("Value");
            }

            Value = _value;
            Suit = _suit;
            Name = $"{Value} of {Suit}";

            switch (Suit)
            {
                case EnumSuit.Spades:
                    ShortSuit = "S";
                    break;

                case EnumSuit.Diamonds:
                    ShortSuit = "D";
                    break;

                case EnumSuit.Clubs:
                    ShortSuit = "C";
                    break;

                case EnumSuit.Hearts:
                    ShortSuit = "H";
                    break;

            }

            switch (Value)
            {
                case EnumValue.Two:
                case EnumValue.Three:
                case EnumValue.Four:
                case EnumValue.Five:
                case EnumValue.Six:
                case EnumValue.Seven:
                case EnumValue.Eight:
                case EnumValue.Nine:
                case EnumValue.Ten:
                    ImageValue = Convert.ToString((int)Value);
                    break;

                case EnumValue.J:
                case EnumValue.Q:
                case EnumValue.K:
                case EnumValue.A:
                    ImageValue = Value.ToString();
                    break;
            }

            Image = $"{ImageValue}{ShortSuit}.png";
        }
    }

    public class Deck
    {
        public Deck()
        {
            Reset();
        }

        public List<Card> Cards { get; set; }

        public void Reset()
        {
            Cards = new List<Card>();

            foreach (EnumSuit Suit in Enum.GetValues(typeof(EnumSuit)))
            {
                foreach (EnumValue Value in Enum.GetValues(typeof(EnumValue)))
                {
                    if (Suit != EnumSuit.Null && Value != EnumValue.Null)
                    {
                        Cards.Add(new Card(Value, Suit));
                    }
                }
            }

            Shuffle();
        }

        public void Shuffle()
        {
            #region source: https://stackoverflow.com/a/4262134
            Cards = Cards.OrderBy(c => Guid.NewGuid()).ToList();
            #endregion
        }

        public Card TakeCard()
        {
            Card card = Cards.First();
            Cards.Remove(card);

            return card;
        }

        public List<Card> TakeCards(int _numberOfCards)
        {
            List<Card> takenCards = new List<Card>();

            for (int i = 0; i < _numberOfCards; i++)
            {
                takenCards.Add(TakeCard());
            }

            return takenCards;
        }
    }


}
