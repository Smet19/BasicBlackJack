﻿
namespace BasicBlackJack
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.Background = new System.Windows.Forms.PictureBox();
            this.resetButton = new System.Windows.Forms.Button();
            this.hitButton = new System.Windows.Forms.Button();
            this.stayButton = new System.Windows.Forms.Button();
            this.playerCardRef = new System.Windows.Forms.PictureBox();
            this.playerHand = new System.Windows.Forms.GroupBox();
            this.dealerHand = new System.Windows.Forms.GroupBox();
            this.dealerCardRef = new System.Windows.Forms.PictureBox();
            this.textOutput = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Background)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerCardRef)).BeginInit();
            this.playerHand.SuspendLayout();
            this.dealerHand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dealerCardRef)).BeginInit();
            this.SuspendLayout();
            // 
            // Background
            // 
            this.Background.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Background.BackColor = System.Drawing.Color.Transparent;
            this.Background.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Background.Image = ((System.Drawing.Image)(resources.GetObject("Background.Image")));
            this.Background.Location = new System.Drawing.Point(1, -1);
            this.Background.Name = "Background";
            this.Background.Size = new System.Drawing.Size(1222, 504);
            this.Background.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Background.TabIndex = 0;
            this.Background.TabStop = false;
            // 
            // resetButton
            // 
            this.resetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.resetButton.Location = new System.Drawing.Point(291, 336);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(75, 23);
            this.resetButton.TabIndex = 2;
            this.resetButton.Text = "Reset";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // hitButton
            // 
            this.hitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.hitButton.Enabled = false;
            this.hitButton.Location = new System.Drawing.Point(372, 336);
            this.hitButton.Name = "hitButton";
            this.hitButton.Size = new System.Drawing.Size(75, 23);
            this.hitButton.TabIndex = 3;
            this.hitButton.Text = "Hit";
            this.hitButton.UseVisualStyleBackColor = true;
            this.hitButton.Click += new System.EventHandler(this.hitButton_Click);
            // 
            // stayButton
            // 
            this.stayButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.stayButton.Enabled = false;
            this.stayButton.Location = new System.Drawing.Point(453, 336);
            this.stayButton.Name = "stayButton";
            this.stayButton.Size = new System.Drawing.Size(75, 23);
            this.stayButton.TabIndex = 4;
            this.stayButton.Text = "Stay";
            this.stayButton.UseVisualStyleBackColor = true;
            this.stayButton.Click += new System.EventHandler(this.stayButton_Click);
            // 
            // playerCardRef
            // 
            this.playerCardRef.Location = new System.Drawing.Point(6, 23);
            this.playerCardRef.Name = "playerCardRef";
            this.playerCardRef.Size = new System.Drawing.Size(71, 96);
            this.playerCardRef.TabIndex = 5;
            this.playerCardRef.TabStop = false;
            this.playerCardRef.Visible = false;
            // 
            // playerHand
            // 
            this.playerHand.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.playerHand.BackColor = System.Drawing.Color.Transparent;
            this.playerHand.Controls.Add(this.playerCardRef);
            this.playerHand.Location = new System.Drawing.Point(291, 365);
            this.playerHand.Name = "playerHand";
            this.playerHand.Size = new System.Drawing.Size(918, 125);
            this.playerHand.TabIndex = 6;
            this.playerHand.TabStop = false;
            this.playerHand.Text = "Player\'s hand";
            // 
            // dealerHand
            // 
            this.dealerHand.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dealerHand.Controls.Add(this.dealerCardRef);
            this.dealerHand.Location = new System.Drawing.Point(291, 12);
            this.dealerHand.Name = "dealerHand";
            this.dealerHand.Size = new System.Drawing.Size(918, 125);
            this.dealerHand.TabIndex = 7;
            this.dealerHand.TabStop = false;
            this.dealerHand.Text = "Dealer\'s Hand";
            // 
            // dealerCardRef
            // 
            this.dealerCardRef.BackColor = System.Drawing.Color.Transparent;
            this.dealerCardRef.Location = new System.Drawing.Point(6, 23);
            this.dealerCardRef.Name = "dealerCardRef";
            this.dealerCardRef.Size = new System.Drawing.Size(71, 96);
            this.dealerCardRef.TabIndex = 6;
            this.dealerCardRef.TabStop = false;
            this.dealerCardRef.Visible = false;
            // 
            // textOutput
            // 
            this.textOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textOutput.Enabled = false;
            this.textOutput.Location = new System.Drawing.Point(12, 336);
            this.textOutput.Multiline = true;
            this.textOutput.Name = "textOutput";
            this.textOutput.Size = new System.Drawing.Size(273, 154);
            this.textOutput.TabIndex = 8;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1221, 502);
            this.Controls.Add(this.textOutput);
            this.Controls.Add(this.dealerHand);
            this.Controls.Add(this.playerHand);
            this.Controls.Add(this.stayButton);
            this.Controls.Add(this.hitButton);
            this.Controls.Add(this.resetButton);
            this.Controls.Add(this.Background);
            this.Name = "MainForm";
            this.Text = "Black Jack";
            ((System.ComponentModel.ISupportInitialize)(this.Background)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerCardRef)).EndInit();
            this.playerHand.ResumeLayout(false);
            this.dealerHand.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dealerCardRef)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Background;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.Button hitButton;
        private System.Windows.Forms.Button stayButton;
        private System.Windows.Forms.PictureBox playerCardRef;
        private System.Windows.Forms.GroupBox playerHand;
        private System.Windows.Forms.GroupBox dealerHand;
        private System.Windows.Forms.PictureBox dealerCardRef;
        private System.Windows.Forms.TextBox textOutput;
    }
}

