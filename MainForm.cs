﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BasicBlackJack
{
    public partial class MainForm : Form
    {
        Deck CurrentDeck = new Deck();

        List<Card> DealerHand = new List<Card>();
        List<Card> PlayerHand = new List<Card>();

        Dictionary<Card, PictureBox> PlayerHandDict = new Dictionary<Card, PictureBox>();
        Dictionary<Card, PictureBox> DealerHandDict = new Dictionary<Card, PictureBox>();

        int PlayerCount = 0;
        int DealerCount = 0;

        public MainForm()
        {
            InitializeComponent();
        }

        private void AddCard(List<Card> _hand, Dictionary<Card, PictureBox> _dict, GroupBox _refGroupBox, ref int _cardCount, bool _hide = false)
        {
            Card CurrentCard = CurrentDeck.TakeCard();

            if(CurrentCard != null)
            {
                if(_dict.TryGetValue(_hand.Last(), out PictureBox RefBox))
                {
                    _hand.Add(CurrentCard);
                    _dict.Add(CurrentCard, CreateCardImage(CurrentCard, RefBox, _refGroupBox, _hide));
                    _cardCount = CalcHand(_hand);
                }
            }
        }

        private void ClearHand(List<Card> _hand, Dictionary<Card, PictureBox> _dict, ref int _cardCount)
        {
            _cardCount = 0;

            foreach (Card CurrentCard in _hand)
            {
                if (_dict.TryGetValue(CurrentCard, out PictureBox CurrentBox))
                {
                    _dict.Remove(CurrentCard);
                    CurrentBox.Dispose();
                }
            }
            _hand.Clear();
        }

        private PictureBox CreateCardImage(Card _card, PictureBox _refPictureBox, GroupBox _refGroupBox, Boolean _hide = false)
        {
            PictureBox NewCard = new PictureBox();

            _refGroupBox.Controls.Add(NewCard);

            NewCard.Size = _refPictureBox.Size;
            NewCard.Location = _refPictureBox.Location;
            NewCard.Location = new Point(NewCard.Location.X + NewCard.Size.Width + 6, NewCard.Location.Y);

            if (_hide)
                NewCard.ImageLocation = "b1fv.png";
            else
                NewCard.ImageLocation = _card.Image;

            return NewCard;
        }

        private int CalcHand(List<Card> _hand)
        {
            int ret = 0;
            bool hasBigAce = false; // If we have 11 score Ace - true

            if (_hand.Count() > 0)
            {
                List<Card> copy = new List<Card>(_hand);

                copy.Sort();
                copy.Reverse();

                foreach (Card card in copy)
                {
                    switch (card.Value)
                    {
                        case EnumValue.Two:
                        case EnumValue.Three:
                        case EnumValue.Four:
                        case EnumValue.Five:
                        case EnumValue.Six:
                        case EnumValue.Seven:
                        case EnumValue.Eight:
                        case EnumValue.Nine:
                        case EnumValue.Ten:
                            ret += (int)card.Value;
                            break;

                        case EnumValue.J:
                        case EnumValue.Q:
                        case EnumValue.K:
                            ret += 10;
                            break;

                        case EnumValue.A:
                            // Checking if we have an ace that is 11
                            if (hasBigAce)
                            {
                                // If we can add one more score to the hand we will
                                //
                                // It's unlikely that this happens since I'm blocking
                                // ability to hit when player or dealer reaches 21
                                // but just in case something funky happens I have this check
                                if (ret < 21)
                                {
                                    ret += 1;
                                }
                                else
                                {
                                    ret -= 9;           // We substract 10 from the frist ace and add 1 for the new one = 9 total
                                    hasBigAce = false;  // And now we don't have an ace that is 11, it's scored as 1 now
                                }
                            }
                            else
                            {   
                                // If we can add full 11 points we do
                                if (ret <= 10)
                                {
                                    ret += 11;
                                    hasBigAce = true;
                                }
                                else
                                {
                                    ret += 1;
                                }

                            }
                            break;
                    }
                }
            }

            return ret;
        }

        #region GameLogic
        private void DealStartingHands()
        {
            Card CurrentCard = CurrentDeck.TakeCard();

            if (CurrentCard != null)
            {
                PlayerHand.Add(CurrentCard);
                PlayerHandDict.Add(CurrentCard, CreateCardImage(CurrentCard, playerCardRef, playerHand));
                PlayerCount = CalcHand(PlayerHand);
            }

            AddCard(PlayerHand, PlayerHandDict, playerHand, ref PlayerCount);

            textOutput.AppendText($"Player has {PlayerCount}" + Environment.NewLine);

            CurrentCard = CurrentDeck.TakeCard();

            if (CurrentCard != null)
            {
                DealerHand.Add(CurrentCard);
                DealerHandDict.Add(CurrentCard, CreateCardImage(CurrentCard, dealerCardRef, dealerHand));
                DealerCount = CalcHand(DealerHand);
            }

            AddCard(DealerHand, DealerHandDict, dealerHand, ref DealerCount, true);

            textOutput.AppendText($"Dealer has {CurrentCard}" + Environment.NewLine);

        }

        private void DealerLogic()
        {
            textOutput.AppendText("Now it's Dealer's turn!" + Environment.NewLine);

            if (DealerHandDict.TryGetValue(DealerHand.Last(), out PictureBox CardBox))
                CardBox.ImageLocation = DealerHand.Last().Image;

            textOutput.AppendText($"Dealer has {DealerCount}" + Environment.NewLine);

            if (DealerCount < 17)
            {
                do
                {
                    AddCard(DealerHand, DealerHandDict, dealerHand, ref DealerCount);
                    textOutput.AppendText($"Dealer has {DealerCount}" + Environment.NewLine);
                } while (DealerCount < 17);
            }
            if (DealerCount < 22)
            {
                if (DealerCount > PlayerCount)
                {
                    textOutput.AppendText("Dealer won!" + Environment.NewLine);
                }
                else if (DealerCount == PlayerCount)
                {
                    textOutput.AppendText("Game ended in a draw!" + Environment.NewLine);
                }
                else
                {
                    textOutput.AppendText("Player won!" + Environment.NewLine);
                }
            }
            else
            {
                textOutput.AppendText("Player won!" + Environment.NewLine);
            }

            resetButton.Enabled = true;
        }
        #endregion

        #region InterfaceLogic
        private void hitButton_Click(object sender, EventArgs e)
        {
            AddCard(PlayerHand, PlayerHandDict, playerHand, ref PlayerCount);
            textOutput.AppendText($"Player has {PlayerCount}" + Environment.NewLine);

            if (PlayerCount > 21)
            {
                resetButton.Enabled = true;
                hitButton.Enabled = false;
                stayButton.Enabled = false;

                textOutput.AppendText("Player busted!" + Environment.NewLine);
            }
            else if (PlayerCount == 21)
            {
                hitButton.Enabled = false;
                stayButton.Enabled = false;

                textOutput.AppendText("Player got blackjack!" + Environment.NewLine);
                DealerLogic();
            }
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            ClearHand(PlayerHand, PlayerHandDict, ref PlayerCount);
            ClearHand(DealerHand, DealerHandDict, ref DealerCount);
            CurrentDeck.Reset();
            textOutput.Text = "";

            DealStartingHands();

            if (PlayerCount != 21)
            {
                hitButton.Enabled = true;
                stayButton.Enabled = true;
                resetButton.Enabled = false;
            }
            else
            {
                textOutput.AppendText("Player got blackjack!" + Environment.NewLine);
                DealerLogic();
            }
        }

        private void stayButton_Click(object sender, EventArgs e)
        {
            hitButton.Enabled = false;
            stayButton.Enabled = false;

            DealerLogic();
        }
        #endregion
    }
}